from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import statusForm
from .models import statusModel


def status(request):
    if request.method == 'POST':
        form = statusForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/')
    
    else:
        form = statusForm()
    obj = statusModel.objects.all().order_by('-date')
    response = {
        'form' : form,
        'posts' : obj,
    }
    return render(request, 'status.html', response)

