from django.urls import path, include
from .views import status

urlpatterns = [
    path('', status, name="statuspage")
]
